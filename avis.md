---
title: Votre avis
---

**Votre avis est précieux!**  

Lors du montage du projet en 2020, nous avions déjà [interrogé plusieurs acteurs](https://docs.google.com/forms/d/e/1FAIpQLSdMOQxcKcwGi1DDj0xUuT7TkdL7-9t9aUe5J7HOO_vccit-RA/viewform) pour initier notre réflexion sur les [cas d'utilisation](https://gitlab.com/open-mobility-indicators/omi-scripts/-/wikis/0.-Cas-d'utilisation). Au cours de l'année 2021, nous interrogeons régulièrement différents types d'acteurs pour voir comment le service et les outils peuvent être utilisés, améliorés...   

Si vous êtes intéressé.e par le projet, n'hésitez pas à [nous contacter via le forum FabMob](https://forum.fabmob.io/t/open-mobility-indicators/220) ou par mail (patgendre94 at gmail.com) pour toute question.   
Merci d'avance !


