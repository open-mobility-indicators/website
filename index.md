---
title: Open Mobility Indicators
---

Open Mobility Indicators est un [ensemble d'outils logiciels libres et collaboratifs](https://gitlab.com/open-mobility-indicators/) qui traite les données ouvertes pour créer des indicateurs de mobilité durable. Les données sont issues d'[OpenStreetMap](https://www.openstreetmap.org/), mais aussi, en France, de [l'Insee](https://www.data.gouv.fr/fr/organizations/institut-national-de-la-statistique-et-des-etudes-economiques-insee/) ou de [transport.data.gouv](https://transport.data.gouv.fr/). La donnée résultant des traitements permet de créer des applications qui rendent visible l'accessibilité à pied d'un quartier ou d'une ville, pour une région ou tout un pays.    
[Pour en savoir plus sur le projet...](https://open-mobility-indicators.gitlab.io/website/projet)

Le projet a commencé par une démonstration sur le territoire de la Région Sud-PACA, développée de septembre à décembre 2020 avec le soutien du Conseil Régional et de la direction régionale de l'ADEME. 

Au 2ème semestre 2021, l'application a été largement réécrite, et généralisér à toute la France métropolitaine, au cours d'un [projet de 6 mois soutenu par l'appel à communs "Résilience des Territoires" de l'Ademe](https://wiki.resilience-territoire.ademe.fr/wiki/Mon_quartier_%C3%A0_pied). Outre le code [publié sur gitlab](https://gitlab.com/open-mobility-indicators/), 
[l'application est en ligne depuis novembre 2021](https://app.openmobilityindicators.org/?b=0&c1=3.8874029999999493%2C43.657118&c2=3.887403%2C43.657118&i=pedestrian-way-types,mon-quartier-a-pied,parcelles-publiques,population-density-from-cycles&p=0&s1=pedestrian-way-types,mon-quartier-a-pied&s2=pedestrian-way-types,mon-quartier-a-pied&split=false&z=13).   

**Mise à jour 2025** : [liste des réutilisations connues](projet?ref_type=heads#réutilisations)

Le projet est toujours hébergé gracieuement par la société multi, mais n'est plus maintenu depuis 2022, [si vous êtes intéressé.e par l'outil et une suite possible au projet, contactez-nous!](avis)

(*nota : le temps d'affichage peut être long - notamment pour la couche "parcelles" qui charge beaucoup de données; c'est pourquoi nous recommandons d'utiliser l'appli sur **Chrome** plutôt que sur Firefox*)

[![Accès la carte Appli OMI](img/clapiers.png "appli OMI")](https://app.openmobilityindicators.org/?b=0&c1=3.8874029999999493%2C43.657118&c2=3.887403%2C43.657118&i=pedestrian-way-types,mon-quartier-a-pied,parcelles-publiques,population-density-from-cycles&p=0&s1=pedestrian-way-types,mon-quartier-a-pied&s2=pedestrian-way-types,mon-quartier-a-pied&split=false&z=13)

Les [livrables du projet](doc) comprennent le code, les données, et des documents présentant le projet. 

Open Mobility Indicators est un projet ouvert, auquel vous pouvez [contribuer](contribuer).   

Merci de votre visite et n'hésitez pas à [nous contacter pour nous faire part de votre avis ou de vos questions](avis), nous cherchons des partenaires pour pérenniser et améliorer l'application !
