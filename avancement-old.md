---
title: Avancement
---

# Démonstrateur 

Le projet a commencé en septembre 2020, grâce au soutien de l’ADEME et du <a href="https://www.maregionsud.fr/">Conseil Régional Provence Alpes Côte-d’Azur</a>. Le territoire pilote est la Région. Le développement a été réalisé [par itérations](https://gitlab.com/open-mobility-indicators/website/-/wikis/avancement/Avancement-it%C3%A9ration-0).
Le démonstrateur a ouvert en janvier 2021, et a été mis à jour au printemps.


## Evolution en 2021

Jailbreak et Patrick Gendre ont porté une [proposition pour améliorer "Mon Quartier à Pied"](https://wiki.resilience-territoire.ademe.fr/wiki/Mon_quartier_%C3%A0_pied) qui a été [retenue pour un financement](https://forum.resilience-territoire.ademe.fr/t/resultats-preliminaires-du-1er-releve-de-communs/601) par l'Appel à Communs [Résilience des Territoires](https://resilience-territoire.ademe.fr/) : le projet d'amélioration de la plateforme OMI et du démonstrateur "mon quartier à pied" commence en juillet, et durera 6 mois. 
Les développements ont consisté à reprendre complètement l'architecture technique pour rendre possible la publication de tout type d'indicateurs, et en parallèle à compléter les indicateurs initiés en 2020 dans le cadre du démonstrateur sur la Région Alpes Provence Côte d'Azur.
Fin octobre, les premiers sont visibles, avec des données pour autour du département de l'Hérault.
L'interface cartographique est encore en développement et sera visible fin novembre.
OMI permet ausssi de calculer des indicateurs classiques sous forme de tableaux, [voici un article qui montre les premiers résultats](_layouts/datawrapper.html).

