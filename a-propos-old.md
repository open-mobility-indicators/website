---
title: À propos
---

# À propos d'Open Mobility Indicators

Open Mobility Indicators est un projet porté par [Patrick Gendre](https://infomobi.frama.site/presentation) pour la partie fonctionnelle et [Jailbreak](https://www.jailbreak.paris/) pour la partie technique et la réalisation.

Le code source est [publié sur gitlab](https://gitlab.com/open-mobility-indicators/data-manipulation/-/wikis/home), **ainsi que [le wiki du projet](https://gitlab.com/open-mobility-indicators/website/-/wikis/home)**.   

Le projet a commencé en septembre 2020, grâce au soutien de l'ADEME et du [Conseil Régional Provence Alpes Côte-d'Azur](https://www.maregionsud.fr/). Le territoire pilote est la Région.

## Territoires pilotes

Nous recherchons un ou des territoires pilotes, commune, intercommunaulité ou départemental, régional, toutes les échelles peuvent être pertinentes,
avec des cas d'usage très variés. 
Usage porté par Jean-Louis Zimmermann, chargé de mission développement territorial au Conseil départemental de Vaucluse et contributeur au sein de l'association WikiCities.

## Historique du projet

Patrick Gendre a travaillé sur l'analyse de marchabilité sur la base de données ouvertes, et notamment encadré plusieurs stages entre 2010 et 2015 au [Cerema](https://www.cerema.fr/fr/cerema), avec des premiers résultats sur Aix-en-Provence convaincants. 
Établi en tant qu'indépendant en 2019, et voyant que l'état de l'art n'a pas significativement avancé depuis sa première expérimentation, il décide de développer ce projet autour de l'idée d'indicateurs de mobilité durable dérivés de données ouvertes, et prend contact avec la [Fabrique des Mobilités](http://lafabriquedesmobilites.fr/) pour faire avancer l'idée.

## Un cofinancement de l'Ademe et de la Région PACA Sud

Le 5 juin, le projet a reçu le feu vert pour un cofinancement par l'Ademe Paca et le Conseil Régional (la décision de co-financement de la Région reste à être confirmée en octobre) et pourra donc démarrer cet été avec des 1ers résultats en septembre.
