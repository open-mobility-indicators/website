---
title: À propos
---
### À propos d'Open Mobility Indicators

*Traiter les données ouvertes pour créer des indicateurs de mobilité durable*

#### Pourquoi ce projet?

[Le projet en 5 minutes](https://gitlab.com/open-mobility-indicators/website/-/blob/master/img/leprojetQAPen5min.pdf)    

Chaque année, des dizaines de voies et chemins se ferment en France. Ces disparitions peu médiatisées et de nombreux aménagements d'infrastructure détériorent peu à peu les déplacements du quotidien à pied ou à vélo et même en transports collectifs, puisqu'il faut faire de grands détours pour atteindre un arrêt de bus. 
De nombreuses études et analyses SIG ont été réalisées par des collectivités, des urbanistes ou par des chercheurs, mais ces résultats sont difficiles à réutiliser.
Il manque une carte interactive et collaborative, et des outils ouverts permettant de mettre en évidence ce problème, ce qui contribuerait à le résoudre.   

Open Mobility Indicators est un ensemble d'outils logiciels libres et collaboratifs qui traite les données ouvertes pour créer des indicateurs de mobilité durable. Les données sont issues d'OpenStreetMap, mais aussi, en France, de [l'Insee](https://www.data.gouv.fr/fr/organizations/institut-national-de-la-statistique-et-des-etudes-economiques-insee/) de [transport.data.gouv](https://transport.data.gouv.fr/). La donnée résultant des traitements permet de créer des applications qui rendent visible l'accessibilité à pied d'un quartier, d'une ville voire d'une région.

<img src="img/overview.png" alt="Les sources de données ouvertes INSEE, OpenStreetMap, datagouv sont retraitées et permettent de créer des applications" height="210" width="670" class="center">

Nous croyons que les informations rendues lisibles par ces outils permettront de sensibiliser les citoyen‧ne‧s, de soutenir les actions des collectivités et des associations, et de mieux orienter les politiques publiques.   

Notre objectif est aussi de faciliter la création de nouvelles couches de données et de nouveaux indicateurs par des personnes qui connaissent les données métier, sans qu'elles aient à se préoccuper du travail technique nécessaire pour produire et publier les données et les cartes, qui est prise en charge par une infrastructure numérique.   
Enfin, notre vision est que ces données et méthodes de calcul et de représentation soient partagées et mutualisées par la communauté, et améliorées collectivement.   

#### Historique du projet

Patrick Gendre a travaillé sur l'analyse de marchabilité sur la base de données ouvertes, et notamment encadré plusieurs stages entre 2010 et 2015 au [Cerema](https://www.cerema.fr/fr/cerema), avec des premiers résultats sur Aix-en-Provence convaincants. 
Établi en tant qu'indépendant en 2019, il décide de développer ce projet autour de l'idée d'indicateurs de mobilité durable dérivés de données ouvertes, et prend contact avec la [Fabrique des Mobilités](http://lafabriquedesmobilites.fr/) pour faire avancer l'idée.

Open Mobility Indicators est un projet porté par [Patrick Gendre](https://infomobi.frama.site/presentation) pour la partie fonctionnelle et [Multi](https://www.multi.coop/blog?item=.%2Ftexts%2Fblog%2Fposts%2Fjailbreak-devient-multi-fr.md) (ex-[Jailbreak](https://www.jailbreak.paris/)) pour la partie technique et la réalisation. 

#### Un démonstrateur soutenu par l'Ademe et la Région Sud PACA en 2020

Le démonstrateur a été dévelopé par itérations de septembre à décembre 2020, grâce au soutien de [l'ADEME](https://www.paca.ademe.fr/) et du [Conseil Régional Provence Alpes Côte-d'Azur](https://www.maregionsud.fr/). Le territoire pilote est la Région.    
La réflexion sur les cas d'usage a été menée avec Jean-Louis Zimmermann, chargé de mission développement territorial au Conseil départemental de Vaucluse et contributeur au sein de l'association [WikiCities](https://twitter.com/WikiCities).   
[Le démonstrateur a ouvert en janvier 2021, et a été complété depuis](https://app.openmobilityindicators.org/?i=dead_ends,omi_score&lat=44.089291&lng=6.242589&z=13).

#### Un Commun open source soutenu par l'AAP "Résilience et Territoires" en 2021

Jailbreak et Patrick Gendre ont porté une [proposition pour améliorer "Mon Quartier à Pied"](https://wiki.resilience-territoire.ademe.fr/wiki/Mon_quartier_%C3%A0_pied) qui a été [retenue pour un financement](https://forum.resilience-territoire.ademe.fr/t/resultats-preliminaires-du-1er-releve-de-communs/601) par l'Appel à Communs [Résilience des Territoires](https://resilience-territoire.ademe.fr/) : le projet d'amélioration de la plateforme OMI et du démonstrateur "mon quartier à pied" se déroule de juillet  décembre 2021. 
Les développements ont consisté à reprendre complètement l'architecture technique pour rendre possible la publication de tout type d'indicateurs, et en parallèle à compléter les indicateurs initiés en 2020 dans le cadre du démonstrateur sur la Région Alpes Provence Côte d'Azur.
  
[L'application carto est en ligne depuis novembre](https://app.openmobilityindicators.org/?b=0&c1=3.8874029999999493%2C43.657118&c2=3.887403%2C43.657118&i=pedestrian-way-types,mon-quartier-a-pied,parcelles-publiques,population-density-from-cycles&p=0&s1=pedestrian-way-types,mon-quartier-a-pied&s2=pedestrian-way-types,mon-quartier-a-pied&split=false&z=13). Elle permet d'[afficher 4 indicateurs](https://gitlab.com/open-mobility-indicators/website/-/wikis/1_visiteur/L%C3%A9gendes-des-indicateurs) liées à la voirie empruntable par les piétons et aux ilots formés par la voirie.    
(*nota : le temps d'affichage peut être long notamment pour la couche "parcelles" qui charge beaucoup de données; il est beaucoup plus rapide sur Chrome que sur Firefox*)

OMI permet ausssi de calculer des indicateurs classiques sous forme de tableaux, [voici un article qui montre les premiers résultats](https://gitlab.com/open-mobility-indicators/website/-/wikis/1_visiteur/Repr%C3%A9senter-des-tableaux-de-donn%C3%A9es).

L'objectif est de rendre désormais possible (et facile) des contributions pour ajouter de nouveaux indicateurs et de nouvelles couches de données. 

#### réutilisations

Voici quelques réutilisations connues de l'application début 2025 :

"Mon quartier à pied / Open Mobility Indicators" a été utile pour le réalisation de deux études du Cerema en Occitanie : 
- l'outil a permis de sensibiliser la commune de Gramat à la question de la perméabilité du réseau piéton et à son impact sur les déplacements à pied et en voiture pour les trajets de courte distance (disponible ici).
- l'outil a également permis de démontrer à la commune de Nogaro (Gers) l'intérêt de réduire certains îlots pour encourager la marche. En croisant cette donnée avec l'outil Urbansimul, j'ai pu identifier le propriétaire de cet îlot et montrer que la parcelle était actuellement en vente. Ces informations ont conforté la commune dans son souhait d'installer l'office de tourisme à proximité et de créer un passage direct pour les randonneurs, évitant ainsi un détour. 

"Mon quartier à pied / Open Mobility Indicators" a été utilisé au Cerema Méditerranée, pour mettre en évidence les freins aux modes actifs générés par les coupures urbaines (et notamment la fermeture résidentielle, enjeu majeur à Marseille - Les espaces résidentiels fermés à Marseille, la fragmentation urbaine devient-elle une norme ? | Cairn.info et ailleurs comme à Nantes Les rues privées : entre banalisation géographique et poches de concentration).

Dans cette perspective, nous avions effectivement pensé l'utiliser dans notre travail sur la pacification des abords des écoles (comprenant le sujet de l'accès aux écoles), afin de montrer l'influence forte de la porosité du tissu urbain sur le potentiel de déploiement de l'écomobilité scolaire. Pacifier les abords des écoles
Finalement, comme la carte était centrée sur le sujet des écoles, nous avons calculé un indicateur spécifique qui rapporte la superficie accessible en 5 minutes à pied autour de l'école par rapport à la superficie accessible dans le même temps "à vol d'oiseau".

Cette mesure de la porosité urbaine / des continuités piétonnes fait aussi partie des éléments nécessaires pour accompagner les collectivités dans leurs stratégies de densification aux abords des arrêts de transport en commun, ou leurs stratégies de soutien aux modes actifs.

Le sujet des continuités piétonnes a été détaillé dans une fiche réalisée en 2020 par le Cerema  : "Les outils fonciers pour rétablir les continuités piétonnes ». Favoriser la marche. Série de fiches : Fiche n° 06 : Les outils fonciers pour rétablir les continuités piétonnes - Cerema.
Un outil visuel comme "Mon quartier à pied / Open Mobility Indicators" (mis à jour et validé collectivement) permettrait de compléter ces recos en facilitant la prise de conscience des collectivités du "coût social" de l'absence de vigilance sur ce sujet (dans les permis de construire, la réception des opérations, la fermeture progressive des chemins ruraux, la fermeture des copropriétés, le manque d'efficacité des TC,  ...)
