---
title: Livrables
---

# Les Livrables

## l'outil en ligne

[La carte interactive (démo pour la région Sud PACA)](https://app.openmobilityindicators.org/) a été ouverte en janvier 2021.
Cette démo sera remplacée d'ici fin 2021 par une nouvelle version complétée couvrant la France entière.

## les données

#### démo 2020
Les données sont publiées dans ce répertoire: [https://files.openmobilityindicators.org/](https://files.openmobilityindicators.org/)
- les données d'entrée (open data) ne sont pas republiées ici, elles sont lues dans chaque script (notebook) : données OpenStreetMap et données de recensement de la population Insee carroyées à 200mx200m
- [données décrivant les impasses du réseau piéton](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/dead_ends/dead_ends.mbtiles) ([fichier texte GeoJSON](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/dead_ends/dead_ends.geojson), [tuiles vecteur mbtiles](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/dead_ends/dead_ends.mbtiles))
- [données décrivant les mailles/ilots du réseau piéton](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/dead_ends/dead_ends.mbtiles), avec densité de population, périmètre, et "score Mon quartier à pied (code couleur)" ([fichier texte GeoJSON](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/omi_score/omi_score.geojson), [tuiles vecteur mbtiles](https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/omi_score/omi_score.mbtiles))
- est également publié sous forme de notebook (format .ipynb) le résultat de la dernière exécution de chacun des 2 scripts, qui apporte notamment des informations sur le temps d'exécution du notebook avec papermill sur notre plateforme
#### OMI 2021
Les données publiées seront publiées d'ici fin 2021, et surtout, seront complétées à mesure que seront développés de nouveaux indicateurs.

## docs et rapports
Le **[wiki du projet](https://gitlab.com/open-mobility-indicators/website/-/wikis/home) est le point d'entrée sur la documentation**. Il sera complété d'ici 2021.    
Il contient également un [panorama des outils sur la marchabilité et la cyclabilité](https://gitlab.com/open-mobility-indicators/website/-/wikis/5_panorama_des_outils_pour_la_marche/1.-Logiciels-et-donn%C3%A9es) et une bibliographie.   
Le wiki de la [démo 2020 est archivé ici](https://gitlab.com/open-mobility-indicators/omi-scripts/-/wikis/home).

## le code

Le projet s'appuie sur des logiciels libres et est entièrement sous licence libre. 
Le code de la démo 2020 est structuré en 3 parties:
- [Calcul des indicateurs](https://gitlab.com/open-mobility-indicators/indicator-notebooks/-/tree/master/notebooks) (effectué dans des notebooks python) 
- [Interface utilisateur](https://gitlab.com/open-mobility-indicators/carto) (cartes interactives) 
- [Déploiement](https://gitlab.com/open-mobility-indicators/omi-ops) 



