--- 
title: Open Mobility Indicators 
--- 

Open Mobility Indicators est un projet ouvert.   

Les contributions sont les bienvenues, à tous les niveaux :   
- [votre avis](https://openmobilityindicators.org/avis) et vos suggestions d'amélioration sur l'application actuelle et sur les indicateurs de marchabilité, merci d'avance!
- il est bien sûr possible de compléter et corriger les données utilisées, notamment les données collaboratives [OpenStreetMap](https://www.openstreetmap.org/)   
- proposer [de nouvelles données, de nouveaux indicateurs](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/Contribuer-aux-indicateurs), des territoires à analyser
- compléter notre [panorama des outils dans le wiki](https://gitlab.com/open-mobility-indicators/website/-/wikis/5_Veille_Outils-pour-la-marche)  
- réutiliser et compléter les [notebooks de calcul des indicateurs](https://gitlab.com/open-mobility-indicators/indicators)   
- réutiliser et adapter le [code applicatif et de déploiement](https://gitlab.com/open-mobility-indicators/website/-/wikis/4_Mainteneur/architecture)    

