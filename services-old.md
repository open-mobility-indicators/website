---
title: Réutilisations
---

# Réutilisations d'Open Mobility Indicators

La prochaine réutilisation sera le 1er projet engagé sur un territoire pilote.
Vous trouverez plus d'éléments [sur le wiki](https://gitlab.com/open-mobility-indicators/omi-scripts/-/wikis/0.-Cas-d'utilisation).

## Votre territoire, vos indicateurs, votre service ?

Vous avez une idée ? Explorez [notre code](https://gitlab.com/open-mobility-indicators/) ou [contactez-nous](https://docs.google.com/forms/d/e/1FAIpQLSdMOQxcKcwGi1DDj0xUuT7TkdL7-9t9aUe5J7HOO_vccit-RA/viewform) !

## Exemple pour le réseau routier à Aix-en-Provence   
Visualisation dans QGIS des routes et chemins qui sont des impasses.
![](img/impasses-aix.png)

Les mailles du réseau viaire colorées selon leur périmètre, 
8000 hab/km2 appparaît comme un seuil pour la zone urbaine dense   
![](img/perimetres.png)

Densités de population des mailles du réseau
![](img/densites.png)

